# Contributing

Before you contribute to this project please read the Format section of the [README](README.md)

## How to contribute

1. Create a fork of this repository with a feature branch
2. Add the tracks you want to contribute in the specified format
3. If you're a newly contributing team add yourself in the contributing section
4. Add links to the tracks you contributed in the team contributions section of the [README](README.md)
5. Submit a Merge request
