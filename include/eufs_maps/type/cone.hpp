#ifndef INCLUDE_EUFS_MAPS_TYPE_CONE_HPP_
#define INCLUDE_EUFS_MAPS_TYPE_CONE_HPP_

#include <Eigen/Dense>

#include <array>
#include <algorithm>
#include <memory>
#include <string>
#include <vector>

#include <eufs_msgs/msg/cone_with_covariance.hpp>
#include <eufs_msgs/msg/cone_array_with_covariance.hpp>
#include <std_msgs/msg/header.hpp>

namespace eufs::map {

enum class Color {
  BLUE,
  YELLOW,
  ORANGE,
  UNKNOWN
};

struct Cone {
  typedef std::shared_ptr<Cone> Ptr;
  typedef std::shared_ptr<const Cone> ConstPtr;

  // (x,y) coordinate of the cone
  Eigen::Vector2d position;

  // Covariance in the position of the cone
  Eigen::Matrix2d covariance;

  /**
   * @brief Construct a new Cone object
   */
  Cone() = default;

  /**
   * @brief Construct a new Cone object
   *
   * @param pos (x,y)-coordinate
   * @param c Color of cone
   */
  explicit Cone(const Eigen::Vector2d &pos, const Color &c = Color::UNKNOWN)
      : position(pos), color(c) {}

  /**
   * @brief Construct a new Cone object
   *
   * @param pos (x,y)-coordinate
   * @param c Color of cone
   */
  explicit Cone(const std::array<double, 2> &pos, const Color &c = Color::UNKNOWN)
      : position(pos.data()), color(c) {}

  /**
   * @brief Construct a new Cone object
   *
   * @param pos (x,y)-coordinate
   * @param cov Covariance of position
   * @param c Color of cone
   */
  Cone(const Eigen::Vector2d &pos, const Eigen::Matrix2d &cov, const Color &c = Color::UNKNOWN)
      : position(pos), covariance(cov), color(c) {}

  /**
   * @brief Construct a new Cone object
   *
   * @param pos (x,y)-coordinate
   * @param cov Covariance of x and y as a flattened array of size 4 (row-wise)
   * @param c Color of cone
   */
  Cone(const std::array<double, 2> &pos, const std::array<double, 4> &cov,
       const Color &c = Color::UNKNOWN)
      : position(pos.data()), covariance(cov.data()), color(c) {}

  /**
   * @brief Construct a new Cone object
   *
   * @param msg eufs_msgs::msg::ConeWithCovariance message
   * @param c Color of cone
   */
  explicit Cone(const eufs_msgs::msg::ConeWithCovariance &msg, const Color &c = Color::UNKNOWN)
      : position(msg.point.x, msg.point.y), covariance(msg.covariance.data()), color(c) {}

  /**
   * @brief Return color of cone
   * @return Color
   */
  Color GetColor() const { return color; }

  /**
   * @brief Return color of cone
   * @return Color
   */  
  Color &GetColor() { return color; }

  /**
   * @brief Convert string to Color enum value
   *
   * @param color Color (blue, yellow, orange, big_orange), otherwise unknown color
   */
  static Color ToColor(const std::string color) {
    if (color == "blue")
      return Color::BLUE;
    else if (color == "yellow")
      return Color::YELLOW;
    else if (color == "orange" || color == "big_orange")
      return Color::ORANGE;
    else
      return Color::UNKNOWN;
  }

 protected:
  // Color of the cone
  Color color = Color::UNKNOWN;
};

template <typename ConeType>
std::vector<ConeType> FromMsg(const eufs_msgs::msg::ConeArrayWithCovariance &msg) {
  auto to_cone = [](const Color &color) {
    return [color](const eufs_msgs::msg::ConeWithCovariance &msg) { return ConeType(msg, color); };
  };
  auto to_blue = to_cone(Color::BLUE);
  auto to_yellow = to_cone(Color::YELLOW);
  auto to_orange = to_cone(Color::ORANGE);
  auto to_unknown = to_cone(Color::UNKNOWN);

  std::vector<ConeType> cones;
  auto it = std::back_inserter(cones);
  std::transform(msg.blue_cones.cbegin(), msg.blue_cones.cend(), it, to_blue);
  std::transform(msg.yellow_cones.cbegin(), msg.yellow_cones.cend(), it, to_yellow);
  std::transform(msg.orange_cones.cbegin(), msg.orange_cones.cend(), it, to_orange);
  std::transform(msg.big_orange_cones.cbegin(), msg.big_orange_cones.cend(), it, to_orange);
  std::transform(msg.unknown_color_cones.cbegin(), msg.unknown_color_cones.cend(), it, to_unknown);

  return cones;
}

template <typename ConeType>
eufs_msgs::msg::ConeWithCovariance ToMsg(const ConeType &cone) {
  eufs_msgs::msg::ConeWithCovariance msg;
  msg.point.x = cone.position(0);
  msg.point.y = cone.position(1);
  msg.covariance = {
      cone.covariance(0, 0),
      cone.covariance(0, 1),
      cone.covariance(1, 0),
      cone.covariance(1, 1),
  };
  return msg;
}

template <typename ConeType>
eufs_msgs::msg::ConeArrayWithCovariance ToMsg(std::vector<ConeType> cones) {
  eufs_msgs::msg::ConeArrayWithCovariance msg;
  for (const auto &cone : cones) {
    switch (cone.GetColor()) {
      case Color::BLUE:
        msg.blue_cones.push_back(ToMsg(cone));
        break;
      case Color::YELLOW:
        msg.yellow_cones.push_back(ToMsg(cone));
        break;
      case Color::ORANGE:
        msg.orange_cones.push_back(ToMsg(cone));
        break;
      default:
        msg.unknown_color_cones.push_back(ToMsg(cone));
        break;
    }
  }
  return msg;
}

template <typename ConeType>
eufs_msgs::msg::ConeArrayWithCovariance ToMsg(const std::vector<ConeType> &cones,
                                              const std_msgs::msg::Header &header) {
  auto msg = eufs::map::ToMsg(cones);
  msg.header = header;
  return msg;
}

}  // namespace eufs::map

#endif // INCLUDE_EUFS_MAPS_TYPE_CONE_HPP_
