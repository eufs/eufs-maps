#ifndef INCLUDE_EUFS_MAPS_TYPE_MAP_HPP_
#define INCLUDE_EUFS_MAPS_TYPE_MAP_HPP_

#include <vector>

namespace eufs::map {

template <typename ConeType>
class Map {
 public:  // Map types
  using InternalRepresentation = std::vector<ConeType>;
  using iterator = typename InternalRepresentation::iterator;
  using const_iterator = typename InternalRepresentation::const_iterator;

 public:  // Constructors
  /**
   * @brief Construct a new Map object
   */
  Map() = default;

  /**
   * @brief Construct a new Map object
   *
   * @param cones List of cones to initialise map with
   */
  explicit Map(const std::vector<ConeType> &cones) : cones_(cones) {}

 public:  // Add methods
  /**
   * @brief Add cone to map
   *
   * @param cone Cone to add
   */
  void Add(const ConeType &cone) { cones_.push_back(cone); }

  /**
   * @brief Add cones to map
   *
   * @param cones Cones to add to map
   */
  void Add(const std::vector<ConeType> &cones) {
    cones_.insert(cones_.end(), cones.begin(), cones.end());
  }

 public:  // Remove methods
  /**
   * @brief Remove cone at given index position
   *
   * @param idx Index position to delete at
   */
  void Remove(const size_t &idx) { cones_.erase(std::next(cones_.begin(), idx)); }

 public:  // Iterators
  /**
   * @brief Returns a read/write iterator that points to the first element.
   *
   * @return Iterator
   */
  iterator begin() { return cones_.begin(); }

  /**
   * @brief Returns a read-only (constant) iterator that points to the first element.
   *
   * @return ConstIterator
   */
  const_iterator begin() const { return cones_.begin(); }

  /**
   * @brief Returns a read-only (constant) iterator that points to the first element.
   *
   * @return ConstIterator
   */
  const_iterator cbegin() const { return cones_.cbegin(); }

  /**
   * @brief Returns a read/write iterator that points one past the last element.
   *
   * @return Iterator
   */
  iterator end() { return cones_.end(); }

  /**
   * @brief Returns a read-only (constant) iterator that points one past the last element.
   *
   * @return ConstIterator
   */
  const_iterator end() const { return cones_.end(); }

  /**
   * @brief Returns a read-only (constant) iterator that points one past the last element.
   *
   * @return ConstIterator
   */
  const_iterator cend() const { return cones_.cend(); }

 public:  // Access methods
  /**
   * @brief Return read-only (constant) reference to cone at given index position
   *
   * @return Cone
   */
  const ConeType &at(size_t idx) const { return cones_.at(idx); }

  /**
   * @brief Return reference to cone at given index position
   *
   * @return Cone
   */
  ConeType &at(size_t idx) { return cones_.at(idx); }

 public:  // Size methods
  /**
   * @brief Returns the number of elements in the map.
   *
   * @return Size
   */
  size_t size() const { return cones_.size(); }

 public:  // Return different representations of map
  /**
   * @brief Return a copy of the map as a vector of Cone objects
   *
   * @return Vector of cones
   */
  std::vector<ConeType> ToCones() const { return cones_; }

 protected:
  InternalRepresentation cones_;
};

}  // namespace eufs::map

#endif // INCLUDE_EUFS_MAPS_TYPE_MAP_HPP_
