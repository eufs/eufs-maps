# EUFS maps

This repository contains competition and real world testing tracks for Formula Student. The tracks are in a format compatible with the [eufs_sim](https://gitlab.com/eufs/simulation_group/eufs_sim).

## Format

The format of the tracks is a simple csv with the following columns: 

* tag - the type of cone (`yellow`, `blue`, `orange`, `big_orange`)
* x - the x coordinate of the cone in meters origin is at the starting position of the car
* y - the y coordinate of the cone in meters origin is at the starting position of the car
* x_variance - the variance of the x coordinate in meters
* y_variance - the variance of the y coordinate in meters
* xy_covariance - the covariance of the x and y coordinates in meters

## Contributors

### [EUFS](https://www.eufs.co/)

* FSUK 2023 autocross
